local json = {}
local cjson = require("cjson")

function json.encode(var)
    local status, result = pcall(cjson.encode, var)
    if status then 
		return result 
	end
end

function json.decode(text)
    local status, result = pcall(cjson.decode, text)
    if status then 
		return result
	end
end

function json.decodec(msg,sz)
    local status, result = pcall(cjson.decodec, msg,sz)
    if status then 
		return result
	end
end

function json.freec(msg,offset)
    local status = pcall(cjson.freec, msg,offset)
    if not status then 
		print("json.freec error")
	end
end

return json
