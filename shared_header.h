//
#ifndef _SHARED_HEADER_
#define _SHARED_HEADER_

#ifdef USE_MEM_HOOK
#include "skynet_malloc.h"
#define my_malloc skynet_malloc
#define my_free skynet_free
#define my_realloc skynet_realloc
#else
#define my_malloc malloc
#define my_free free
#define my_realloc realloc
#endif

#endif

