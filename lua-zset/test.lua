local zset = require "zset"

local total = 100
local all = {}
for i=1, total do
    all[#all + 1] = i
end

local function random_choose(t)
    if #t == 0 then
        return
    end
    local i = math.random(#t)
    return table.remove(t, i)
end

local zs = zset.new()

while true do
    local score = random_choose(all)
    if not score then
        break
    end
    local name = "a" .. score
    zs:add(score, name)
end

assert(total == zs:count())

print("rank 28:", zs:rank("a28"))
print("rev rank 28:", zs:rev_rank("a28"))

local t = zs:range(1, 10)
print("rank 1-10:")
for _, name in ipairs(t) do
    print(name)
end

local t = zs:rev_range(1, 10)
print("rev rank 1-10:")
for _, name in ipairs(t) do
    print(name)
end

print("------------------ dump ------------------")
zs:dump()

print("------------------ dump after limit 10 ------------------")
zs:limit(10)
zs:dump()

print("------------------ dump after rev limit 5 ------------------")
zs:rev_limit(5)
zs:dump()

local zt = zset.new()
local score = 10
for i=1,10 do
    zt:add(i, "i======" .. i)
end

zt:add(0, "i")
zt:add(1, "i=")
zt:add(3, "i==")
zt:add(5, "i===")
zt:add(7, "i===")
zt:add(10, "i====")
zt:add(11, "i=====")

zt:add(score, "1")
zt:add(score, "2")
zt:add(score, "6")
zt:add(score, "7")
zt:add(score, "8")
zt:add(score, "3")
zt:add(score, "4")
zt:add(score, "5")
zt:add(score, "9")
zt:add(score, "10")
print("------------------ zt dump ------------------")
zt:dump()

print("------------------ dump after rev limit 5 ------------------")
zt:rev_limit(5)
zt:dump()
