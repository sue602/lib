--
-- Author: SuYinXiang (sue602@163.com)
-- Date: 2016-04-05 16:55:56
--

require "sharefunc"
local skynet = require "skynet"
local profile = require "skynet.profile"
local serviceAPI = require "ServiceAPI"

local nodeid,instance = ...

--超时时间
local OPT_TIME_OUT = 1

local SEND_CMD = {
    set = "set",
    del = "delete",
}

local redisCmd = require "RedisCmd"

skynet.start(function()
    
    --注册dispatch函数
    skynet.dispatch("lua", function(_,_,cmd, ...)
        profile.start()

        local f = assert(redisCmd[cmd])
        if SEND_CMD[cmd] then
            cmd = SEND_CMD[cmd]
            f(...)
        else
            local ok,ret = xpcall(f,sharefunc.exception,...)
            skynet.ret(skynet.pack(ok,ret))
        end

        local time = profile.stop()
        if time > 3 then
            local p = ti[cmd]
            if p == nil then
                p = { n = 0, ti = 0 }
                ti[cmd] = p
            end
            p.n = p.n + 1
            p.ti = p.ti + time
        end

    end)

    -- 注册 info 函数，便于 debug 指令 INFO 查询。
    skynet.info_func(function()
        dump(ti, "redis ti=", 10)
        return ti
    end)

    serviceAPI.setSvr(skynet.self(),serviceAPI.redisGame,nodeid,instance)
end)
