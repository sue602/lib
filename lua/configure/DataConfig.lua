local skynet = require "skynet"
local builder = require "skynet.datasheet.builder"
local datasheet = require "skynet.datasheet"

local localdata = require "LocalData"
local DataConfig = {}

local function set(key, value,update)
  assert("string" == type(key), "key must string!")
  assert(value, "value is nil!")
  if "table" == type(value) then
      if not next(value) then
          skynet.error(string.format("local data \"%s\" is empty!", key))
      end
    end
    if update then
      builder.update(key, value)
    else
      builder.new(key, value)
    end
end

function DataConfig.load( update )
  set("MY_KEY",localdata.test(),update)
end

function DataConfig.query( key )
  return datasheet.query(key)
end

function DataConfig.set( key, value )
  set(key,value,false)
end

function DataConfig.update( key, value )
  set(key,value,true)
end

return DataConfig