--[[
	服务地址管理
]]
local skynet = require "skynet.manager"
local cluster = require("skynet.cluster")
local dataconfig = require("DataConfig")

local ServiceAPI = {
	watchdog	= ".watchdog_%d",--看门狗服务
	gate		= ".gate_%d",--网关服务
	redisGame 	= ".redisGame_%d_%d",--redis服务
	mysqlGame 	= ".mysqlGame_%d_%d",--mysql服务
	agent		= ".agent_%d_%d",--agent服务
	log			= ".log_%d_%d",--log服务
	ranking		= ".ranking_%d_%d",--排行榜服务
	esservice	= ".esservice_%d_%d",--elasticsearch服务
	routerserver= ".routerserver_%d_%d",--路由服务
	chatserver	= ".chatserver_%d_%d",--聊天服务
	cacheserver	= ".cacheserver_%d_%d",--缓存服务
	auctionserver = ".auctionserver_%d_%d"--拍卖服务
}

--cluster配置
ServiceAPI.CLUSTER_CFG = "CLUSTER_CFG"
--游戏服配置
ServiceAPI.GAME_CFG = "GAME_CFG"

--[[
设置服务地址
@return 无
]]
function ServiceAPI.setSvr(address,key,index1,index2)
	if index1 and index2 then
		key = string.format(key,index1,index2)
	elseif index1 then
		key = string.format(key,index1)
	elseif index2 then
		key = string.format(key,index2)
	end
	skynet.name(key,address)
end

--[[
获取服务地址
@return 本节点服务地址
]]
function ServiceAPI.getSvr(key,index1,index2)
	if index1 and index2 then
		key = string.format(key,index1,index2)
	elseif index1 then
		key = string.format(key,index1)
	elseif index2 then
		key = string.format(key,index2)
	end
	--获取本节点服务地址
	local address = skynet.localname(key)
	return address
end

--[[
call服务
]]
function ServiceAPI.call( address,command, ... )
	return skynet.call(address,"lua", command, ...)
end

--[[
send服务
]]
function ServiceAPI.send( address, command, ... )
	skynet.send(address,"lua", command, ...)
end

--[[
读取cluster配置
]]
function ServiceAPI.loadclusterscfg(nodeid,update)
	local clusterQuerySql = "SELECT * FROM clusters"
	local addr = ServiceAPI.getSvr(ServiceAPI.mysqlGame,nodeid,0)
	local ok,result = ServiceAPI.call(addr,"execute",clusterQuerySql)
	local nodecfg = {}
	if ok and result then
		for i,node in ipairs(result) do
			nodecfg[tostring(node.nodeid)] = node
		end
		--设置到datasheet
		if not update then
			dataconfig.set( ServiceAPI.CLUSTER_CFG, nodecfg)
		else
			dataconfig.update( ServiceAPI.CLUSTER_CFG, nodecfg)
		end
	end
end

--[[
reload cluster配置
]]
function ServiceAPI.reloadclusters()
	local nodecfg = dataconfig.query(ServiceAPI.CLUSTER_CFG)
	local clustercfg = {}
	if nodecfg then
		for key,node in pairs(nodecfg) do
			clustercfg[node.nodename] = node.ip .. ":" .. node.port
		end
		dump(clustercfg,"clustercfg==",10)
		cluster.reload(clustercfg)
	end
end

--[[
读取游戏服配置
]]
function ServiceAPI.loadgamescfg(nodeid,update)
	local gameQuerySql = "SELECT * FROM gameconf"
	local addr = ServiceAPI.getSvr(ServiceAPI.mysqlGame,nodeid,0)
	local ok,result = ServiceAPI.call(addr,"execute",gameQuerySql)
	local gamecfg = {}
	if ok and result then
		for i,node in ipairs(result) do
			gamecfg[tostring(node.serverid)] = node
		end
		--设置到datasheet
		if not update then
			dataconfig.set( ServiceAPI.GAME_CFG, gamecfg)
		else
			dataconfig.update( ServiceAPI.GAME_CFG, gamecfg)
		end
	end
end

function ServiceAPI.queryCluster( nodeid )
	local cfg = dataconfig.query( ServiceAPI.CLUSTER_CFG )
	if cfg and nodeid then
		nodeid = tonumber(nodeid)
		for tmpNodeid,item in pairs(cfg) do
			if tonumber(tmpNodeid) == nodeid then
				return item
			end
		end
	end
	skynet.error("ServiceAPI.queryCluster error ",nodeid,cfg)
	return nil
end

--- 通过类型查询cluster信息
function ServiceAPI.queryClusterBy( mType )
	local cfg = dataconfig.query( ServiceAPI.CLUSTER_CFG )
	local ret = {}
	if cfg and mType then
		mType = tonumber(mType)
		for tmpNodeid,item in pairs(cfg) do
			if tonumber(item.type) == mType then
				table.insert(ret,item)
			end
		end
	end
	skynet.error("ServiceAPI.queryClusterBy error ",cfg)
	return ret
end

--- 查询游戏配置信息
function ServiceAPI.queryGame( serverid )
	serverid = tonumber(serverid)
	local cfg = dataconfig.query( ServiceAPI.GAME_CFG )
	if cfg and serverid then
		for tmpserverid,item in pairs(cfg) do
			if tonumber(tmpserverid) == serverid then
				return item
			end
		end
	end
	skynet.error("ServiceAPI.queryGame error ",serverid,cfg)
	return nil
end

---通过节点查询有多少个王国
function ServiceAPI.queryGames( nodeid )
	local cfg = dataconfig.query( ServiceAPI.GAME_CFG )
	local games = {}
	if cfg and nodeid then
		local nodeid = tonumber(nodeid)
		for tmpserverid,item in pairs(cfg) do
			if tonumber(item.nodeid) == nodeid then
				games[tmpserverid] = item
			end
		end
	end
	return games
end

--[[
获取跨节点服务地址
]]
function ServiceAPI.getClusterProxy( nodeid,svrname )
	local clusterconf = ServiceAPI.queryCluster(nodeid)
	assert(nil ~= clusterconf,"ServiceAPI.getClusterProxy config is nil")
	local nodesvr = cluster.query(clusterconf.nodename, svrname)
	local proxy = cluster.proxy(clusterconf.nodename, nodesvr)
	return proxy
end

--[[
查询跨节点服务
]]
function ServiceAPI.getClusterSvr( nodeid,svrname )
	local clusterconf = ServiceAPI.queryCluster(nodeid)
	assert(nil ~= clusterconf,"ServiceAPI.getClusterSvr config is nil")
	local nodesvr = cluster.query(clusterconf.nodename, svrname)
	return nodesvr
end

--[[
打开cluster服务
]]
function ServiceAPI.opencluster( nodeid )
	local nodename = ServiceAPI.queryCluster( nodeid ).nodename
	assert(nil ~= nodename,"nodename is nil " .. nodeid)
	cluster.open(nodename)
end

--[[
cluster send
]]
function ServiceAPI.clustersend( nodeid, svrname, cmd, ...)
	local clusterSvr = ServiceAPI.getClusterSvr( nodeid, string.format(svrname,nodeid) )
	local nodename = ServiceAPI.queryCluster( nodeid ).nodename
	cluster.send(nodename,clusterSvr,cmd,...)
end

return ServiceAPI
